const Web3 = require('web3')
var greeter = require('./greeter.json')
var economy = require('./Economy.json')
exports.handler = function(event, context, callback) {
	console.log(event)
	//var web3= new Web3('http://107.23.87.210:8545');
	var provider = new Web3.providers.HttpProvider('http://107.23.87.210:8545')
	web3 = new Web3(provider)
	console.log(web3.version.network)
	//console.log(web3.version)
	//web3.eth.getAccounts().then(console.log);

	const contract = require('truffle-contract')
	//const greeter_contract = contract(greeter)
	const economy_contract = contract(economy)

	//greeter_contract.setProvider(web3.currentProvider)
	economy_contract.setProvider(web3.currentProvider)
	var result
	web3.eth.getAccounts((error, accounts) => {
		//console.log(greeter_contract)
		//greeter_contract.deployed
		economy_contract.deployed().then((instance) => {
        		instance.getMemberScore(event['pathParameters']['uid']).then((res) => {
			//instance.greet().then((res) => {
          		console.log(res);
			result = res;
			var response = {
				"statusCode": 200,
				"headers": { "Content-Type": "application/json"  },
				"body": {score: res[2]}
			}
			callback(null, response)
        		})
      		})


	});

  // or
};
